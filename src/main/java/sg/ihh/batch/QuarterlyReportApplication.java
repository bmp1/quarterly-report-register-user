package sg.ihh.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import sg.ihh.batch.job.TerminateBean;
import sg.ihh.batch.util.log.AppLogger;


@EnableBatchProcessing
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class QuarterlyReportApplication implements CommandLineRunner {

    private static AppLogger log = new AppLogger(QuarterlyReportApplication.class);

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    private Job quarterlyReportJob;

    public static void main(String[] args) {
        System.out.println("Spring Boot application starting");
        ConfigurableApplicationContext ctx = SpringApplication.run(QuarterlyReportApplication.class, args);
        ctx.getBean(TerminateBean.class);
        ctx.close();
    }

    public void run(String... args) throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException, JobParametersInvalidException, JobRestartException {

        jobLauncher.run(quarterlyReportJob, new JobParameters());

    }
}
package sg.ihh.batch.manager;

import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPConnectionPool;
import com.unboundid.ldap.sdk.LDAPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import sg.ihh.batch.config.BaseConfig;
import sg.ihh.batch.util.property.LdapProperty;

import javax.annotation.PostConstruct;

@Configuration
public class LDAPConnectionManager extends BaseConfig {

    private LDAPConnectionPool connectionPool;
    private String host;
    private int port;

    @Autowired
    private LdapProperty ldapProperty;

    public LDAPConnectionManager() {
        log = getLogger(this.getClass());

    }

    @PostConstruct
    private void init() {
        final String methodName = "init";
        start(methodName);
        initConnection();
        completed(methodName);
    }


    public LDAPConnection getServiceConnection() {
        LDAPConnection connection = null;
        try {
            connection = connectionPool.getConnection();
        } catch (LDAPException ex) {
            log.error("getConnection", ex.getMessage());
        }
        return connection;
    }

    public LDAPConnectionOptions getDefaultConnectionOptions() {
        LDAPConnectionOptions options = new LDAPConnectionOptions();
        options.setBindWithDNRequiresPassword(false);
        options.setConnectTimeoutMillis(30000);
        options.setFollowReferrals(false);
        return options;
    }

    public void initConnection() {
        final String methodName = "Constructor";
        start(methodName);

        host = ldapProperty.getHost();
        port = ldapProperty.getPort();

        log.info("Initiating LDAP Connection to : " + host + ":" + port);

        String bindDn = ldapProperty.getBindDn();
        String bindPw = EncryptionManager.getInstance().decrypt(ldapProperty.getBindPassword());

        int minConnection = ldapProperty.getConnectionsMin();
        int maxConnection = ldapProperty.getConnectionsMax();

        LDAPConnectionOptions connectionOptions = getDefaultConnectionOptions();

        try (LDAPConnection connection = new LDAPConnection(connectionOptions, host, port)) {
            connection.bind(bindDn, bindPw);
            connectionPool = new LDAPConnectionPool(connection, minConnection, maxConnection);
        } catch (LDAPException ex) {
            log.error("Constructor", ex);
        }
        completed(methodName);
    }

}
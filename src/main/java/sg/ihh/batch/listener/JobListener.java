package sg.ihh.batch.listener;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sg.ihh.batch.util.constant.Constants;
import sg.ihh.batch.util.log.AppLogger;

@Component
public class JobListener extends JobExecutionListenerSupport {

    private AppLogger log;

    private String jobId;

    JobParameters jobParameters;

    @Autowired
    public JobListener() {
        log = new AppLogger(this.getClass());
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {

        jobParameters = jobExecution.getJobParameters();
        String methodName = "beforeJob";
        jobId = jobParameters.getString(Constants.PARAM_JOB_ID);


        log.debug(methodName, Constants.START + " a job - " + jobId);
    }

    @Override
    public void afterJob(JobExecution jobExecution) {

        String methodName = "afterJob";
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.debug(methodName, "Job " + jobId + " is " + Constants.END);
        } else {
            log.debug(methodName, "Job " + jobId + " is " + jobExecution.getStatus());
        }
        jobExecution.stop();
    }
}

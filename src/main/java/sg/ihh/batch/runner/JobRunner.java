package sg.ihh.batch.runner;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import sg.ihh.batch.util.constant.Constants;
import sg.ihh.batch.util.log.AppLogger;

@Component
public class JobRunner {

    private static final AppLogger log = new AppLogger(JobRunner.class);
    
    private JobLauncher launcher;
    private Job gcMigrationJob;
    
    
    @Autowired
    public JobRunner(Job gcMigrationJob, JobLauncher launcher) {
    	this.gcMigrationJob = gcMigrationJob;
    	this.launcher = launcher;
    }
    
    
    @Async
    public void runBatchJob() {
    	String jobId = String.valueOf(System.currentTimeMillis());
    	// Add JobId to Log to prevent same job running
    	ThreadContext.put("jobId", jobId);
    	
    	JobParametersBuilder builder = new JobParametersBuilder();
    	// To allow rerun the job.
    	builder.addString(Constants.PARAM_JOB_ID, jobId);

    	runJob(gcMigrationJob, builder.toJobParameters());
    }

    
	private void runJob(Job job, JobParameters parameters) {
		final String methodName = "runJob";
		try {

		    launcher.run(job, parameters);
		
		} catch (JobExecutionAlreadyRunningException e) {
			log.info(methodName, "Job with fileName={} is already running.");
		} catch (JobRestartException e) {
			log.info(methodName, "Job with fileName={} was not restarted.");
		} catch (JobInstanceAlreadyCompleteException e) {
			log.info(methodName, "Job with fileName={} already completed.");
		} catch (JobParametersInvalidException e) {
			log.info(methodName, "Invalid job parameters.");
		}
	}
    
    
}


package sg.ihh.batch.controller;

import com.sun.org.apache.bcel.internal.generic.FieldOrMethod;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResultEntry;
import sg.ihh.batch.controller.base.BaseController;
import sg.ihh.batch.manager.LDAPConnectionManager;
import sg.ihh.batch.model.LDAPAttribute;
import sg.ihh.batch.model.User;
import sg.ihh.batch.util.helper.DateHelper;
import sg.ihh.batch.util.ldap.LDAPHelper;
import sg.ihh.batch.util.property.LdapProperty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LdapController extends BaseController {

    private LDAPConnectionManager ldapConnectionManager;

    private LdapProperty ldapProperty;

    public LdapController(LdapProperty ldapProperty, LDAPConnectionManager ldapConnectionManager) {
        log = getLogger(this.getClass());

        this.ldapProperty = ldapProperty;
        this.ldapConnectionManager = ldapConnectionManager;
    }

    public List<User> downloadUser(String ldapStartOfDay, String ldapEndOfDay) {
        String methodName = "downloadUser";
        start(methodName);
        List<User> users = new ArrayList<>();

        // Construct the date range filter
        Filter filter = Filter.createANDFilter(
                Filter.createGreaterOrEqualFilter("createTimeStamp", ldapStartOfDay),
                Filter.createLessOrEqualFilter("createTimeStamp", ldapEndOfDay)
        );

        log.debug(methodName, "Filter : " + filter);
        users = paginatedDownloadUser(filter);
        completed(methodName);
        return users;
    }

    private List<User> paginatedDownloadUser(Filter filter) {
        String methodName = "paginatedDownloadUser";
        List<User> users = new ArrayList<>();

        try (LDAPConnection conn = ldapConnectionManager.getServiceConnection()) {


            List<SearchResultEntry> entries = LDAPHelper.paginationSearch(conn, filter, ldapProperty.getBaseDn(), SearchRequest.ALL_USER_ATTRIBUTES);

            for (SearchResultEntry entry : entries) {
                User user = new User();
                user.setMail(entry.getAttributeValue(LDAPAttribute.MAIL));
                user.setPplID(entry.getAttributeValue(LDAPAttribute.NRIC));
                LocalDateTime ldapCreateDt = DateHelper.parseLdapFormat(entry.getAttributeValue(LDAPAttribute.CREATE_TIME_STAMP));
                user.setCreateTimestamp(DateHelper.formatExcelDatestamp(ldapCreateDt));

                users.add(user);
            }
        } catch (Exception ex) {
            log.error(methodName, ex.getMessage());
        }
        return users;
    }
}

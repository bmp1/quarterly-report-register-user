package sg.ihh.batch.tasklet;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.item.ExecutionContext;
import sg.ihh.batch.util.constant.Constants;
import sg.ihh.batch.util.helper.PropertyHelper;
import sg.ihh.batch.util.helper.StringHelper;
import sg.ihh.batch.util.log.AppLogger;

public class BaseTasklet implements StepExecutionListener {

    protected AppLogger log;

    protected AppLogger getLogger(Class<?> clazz) {
        return new AppLogger(clazz);
    }

    protected void start(String id, String methodName) {
        log.debug(id, methodName, Constants.START);
    }

    protected void completed(String id, String methodName) {
        log.debug(id, methodName, Constants.END);
    }

    protected void start(String methodName) {
        log.debug(methodName, Constants.START);
    }

    protected void completed(String methodName) {
        log.debug(methodName, Constants.END);
    }

    protected static String getProperty(String key) {
        return PropertyHelper.getProperty(key);
    }

    public boolean validate(String str) {
        return StringHelper.validate(str);
    }

    // StepExecutionListener
    @Override
    public void beforeStep(StepExecution stepExecution) {
        log.debug(Constants.START);
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        log.debug(Constants.END);
        return ExitStatus.COMPLETED;
    }

    ExecutionContext getExecutionContext(ChunkContext chunkContext) {
        return chunkContext.getStepContext()
                .getStepExecution()
                .getJobExecution()
                .getExecutionContext();
    }
}

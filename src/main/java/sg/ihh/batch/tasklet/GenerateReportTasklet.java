package sg.ihh.batch.tasklet;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import sg.ihh.batch.model.Ranges;
import sg.ihh.batch.model.User;
import sg.ihh.batch.util.helper.FileHelper;
import sg.ihh.batch.util.property.ApplicationProperty;
import sg.ihh.batch.util.property.QuarterRangeProperty;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class GenerateReportTasklet extends BaseTasklet implements Tasklet {

    private Ranges quarterRanges;

    private ApplicationProperty applicationProperty;
    private QuarterRangeProperty quarterRangeProperty;

    private List<User> userList = new ArrayList<>();

    public GenerateReportTasklet( Ranges quarterRanges, List<User> userList) {
        log = getLogger(this.getClass());
        this.quarterRanges = quarterRanges;
        this.userList = userList;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return ExitStatus.COMPLETED;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {

        if(!userList.isEmpty()) {
            createOutputFile();
            exportUserXLS(userList);
        }
        return RepeatStatus.FINISHED;
    }


    public void createOutputFile() {
        String methodName = "createOutputFile";
        start(methodName);
        FileHelper.createDirectory(quarterRanges.getFileOutputLocation());
        FileHelper.createOutputFile(quarterRanges.getFileOutputLocation(), quarterRanges.getFileOutputName());
        completed(methodName);
    }

    private void exportUserXLS(List<User> userList) {
        String methodName = "exportUserXLS";
        start(methodName);
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet spreadsheet = workbook.createSheet(quarterRanges.getQuarter() + " Users");
        Map<String, Object[]> infoData = new TreeMap<String, Object[]>();

        int row = 1;
        //set header
        infoData.put(String.valueOf(row), new Object[]{"Email", "NRIC/Passport/FIN", "Joined Date"});

        for (User user : userList) {
            row++;
            infoData.put(String.valueOf(row), new Object[]{user.getMail(), user.getPplID(), user.getCreateTimestamp()});
        }

        Set<String> keyid = infoData.keySet();
        int rowid = 0;

        // writing the data into the sheets...
        for (String key : keyid) {
            XSSFRow xssfRow = spreadsheet.createRow(rowid++);
            Object[] objectArr = infoData.get(key);
            int cellid = 0;

            for (Object obj : objectArr) {
                Cell cell = xssfRow.createCell(cellid++);
                cell.setCellValue((String) obj);

                CellStyle wrapStyle = workbook.createCellStyle();
                wrapStyle.setWrapText(true);
                wrapStyle.setVerticalAlignment(VerticalAlignment.TOP);
                cell.setCellStyle(wrapStyle);

            }
            xssfRow.setHeightInPoints((1 * spreadsheet.getDefaultRowHeightInPoints()));
        }
        spreadsheet.autoSizeColumn(0);
        spreadsheet.autoSizeColumn(1);
        spreadsheet.autoSizeColumn(2);

        // .xlsx is the format for Excel Sheets...
        // writing the workbook into the file...
        try {
            FileOutputStream out = new FileOutputStream(new File(quarterRanges.getFileOutputLocation() + quarterRanges.getFileOutputName()));
            workbook.write(out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        completed(methodName);
    }

}

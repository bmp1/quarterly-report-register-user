package sg.ihh.batch.tasklet;

import org.apache.juli.logging.Log;
import org.apache.tomcat.jni.Local;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import sg.ihh.batch.controller.LdapController;
import sg.ihh.batch.manager.LDAPConnectionManager;
import sg.ihh.batch.model.Ranges;
import sg.ihh.batch.model.User;
import sg.ihh.batch.util.helper.DateHelper;
import sg.ihh.batch.util.property.LdapProperty;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.List;

public class DownloadUserFromLdapTasklet extends BaseTasklet implements Tasklet {

    private Ranges quarterRanges;
    private LdapProperty ldapProperty;
    private List<User> userList;

    private LdapController ldapController;
    private LDAPConnectionManager ldapConnectionManager;

    public DownloadUserFromLdapTasklet(Ranges quarterRanges,LdapProperty ldapProperty,LDAPConnectionManager ldapConnectionManager, List<User> userList) {
        log = getLogger(this.getClass());
        this.quarterRanges = quarterRanges;
        this.ldapProperty = ldapProperty;
        this.ldapConnectionManager = ldapConnectionManager;
        this.ldapController = new LdapController(ldapProperty, ldapConnectionManager);
        this.userList = userList;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return ExitStatus.COMPLETED;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
        downloadUserFromLDAP();
        return RepeatStatus.FINISHED;
    }

    private void downloadUserFromLDAP() {
        final  String methodName = "downloadUserFromLDAP";
        start(methodName);
        LocalDateTime currentDateTime = LocalDateTime.now();
        Month startMonth = Month.valueOf(quarterRanges.getStartMonth());
        Month endMonth = Month.valueOf(quarterRanges.getEndMonth());

        LocalDateTime startOfDay = currentDateTime.withYear(Integer.parseInt(quarterRanges.getYear())).withMonth(startMonth.getValue()).withDayOfMonth(1).with(LocalTime.MIDNIGHT);
        log.debug(methodName, "Start date : " + DateHelper.formatDateTime(startOfDay));
        String ldapStartOfDay = DateHelper.formatLDAPFormatter(startOfDay);


        LocalDateTime endOfDay = currentDateTime.withYear(Integer.parseInt(quarterRanges.getYear())).withMonth(endMonth.getValue()).withDayOfMonth(Month.of(endMonth.getValue()).maxLength()).with(LocalTime.MAX);
        log.debug(methodName, "End date : " + DateHelper.formatDateTime(endOfDay));
        String ldapEndOfDay = DateHelper.formatLDAPFormatter(endOfDay);

        List<User> users = ldapController.downloadUser(ldapStartOfDay, ldapEndOfDay);
        log.debug(methodName, "Users found : " + users.size());

        userList.addAll(users);
        completed(methodName);
    }
}

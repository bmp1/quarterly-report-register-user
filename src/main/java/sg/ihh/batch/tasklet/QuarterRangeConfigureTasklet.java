package sg.ihh.batch.tasklet;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import sg.ihh.batch.model.Ranges;
import sg.ihh.batch.model.User;
import sg.ihh.batch.util.constant.Constants;
import sg.ihh.batch.util.helper.DateHelper;
import sg.ihh.batch.util.json.JsonUtil;
import sg.ihh.batch.util.property.ApplicationProperty;
import sg.ihh.batch.util.property.QuarterRangeProperty;

import java.time.LocalDateTime;
import java.util.List;

public class QuarterRangeConfigureTasklet extends BaseTasklet implements Tasklet {

    private Ranges quarterRanges;

    private ApplicationProperty applicationProperty;
    private QuarterRangeProperty quarterRangeProperty;
    private List<User> userList;

    public QuarterRangeConfigureTasklet(ApplicationProperty applicationProperty, QuarterRangeProperty quarterRangeProperty,
                                        Ranges quarterRanges, List<User> userList) {
        log = getLogger(this.getClass());
        this.applicationProperty = applicationProperty;
        this.quarterRangeProperty = quarterRangeProperty;
        this.quarterRanges = quarterRanges;
        this.userList = userList;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return ExitStatus.COMPLETED;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) {
        quarterRangeConfigure();
        return RepeatStatus.FINISHED;
    }

    private void quarterRangeConfigure() {

        String methodName = "quarterRangeConfigure";
        start(methodName);

        // Generate the report date
        LocalDateTime ldt = LocalDateTime.now();
        String createDt = DateHelper.formatDatestamp(ldt);
        int currentMonthValue = ldt.getMonthValue();

        quarterRanges.setCreateDt(createDt);

        // build quarterRanges
        //logic, if current month is October to December, it will generate report for Q3 (July-september)
        boolean customRange = quarterRangeProperty.isQuarterCustom();
        log.debug(methodName, "Custom Quarter enable : " + customRange);
        String currentQuarter = "";

        if (customRange) {
            String customValue = quarterRangeProperty.getQuarterCustomValue();
            quarterRanges = quarterRangesBuilder(customValue, quarterRanges);
            quarterRanges.setYear(quarterRangeProperty.getQuarterCustomYear());

        } else {
            if (currentMonthValue >= 1 && currentMonthValue <= 3) {
                currentQuarter = "Q4";
            } else if (currentMonthValue >= 4 && currentMonthValue <= 6) {
                currentQuarter = "Q1";
            } else if (currentMonthValue >= 7 && currentMonthValue <= 9) {
                currentQuarter = "Q2";
            } else if (currentMonthValue >= 10 && currentMonthValue <= 12) {
                currentQuarter = "Q3";
            }
            log.debug(methodName, "Generate Quarter : " + currentQuarter);

            String year = String.valueOf(ldt.getYear());
            quarterRanges = quarterRangesBuilder(currentQuarter, quarterRanges);
            quarterRanges.setYear(year);
        }

        String outputFileName = fileNameBuilder(quarterRanges.getQuarter(), quarterRanges.getStartMonth(),
                quarterRanges.getEndMonth(), quarterRanges.getYear(), quarterRanges.getCreateDt());

        quarterRanges.setFileOutputName(outputFileName);
        quarterRanges.setFileOutputLocation(applicationProperty.getOutputLocation());

        log.debug(methodName, JsonUtil.toJson(quarterRanges));
        completed(methodName);

    }

    private String fileNameBuilder(String quarter, String startMonth, String endMonth, String year, String generatedDate) {
        String fileName;
        String fileNameFormat = applicationProperty.getOutputFilename();
        fileName = fileNameFormat.replace("{quarter}", quarter)
                .replace("{from}", startMonth)
                .replace("{to}", endMonth)
                .replace("{year}", year)
                .replace("{generateDate}", generatedDate);

        return fileName;
    }

    private Ranges quarterRangesBuilder(String quarter, Ranges quarterRanges) {
        String startMonth = "";
        String endMonth = "";

        switch (quarter) {
            case Constants.QUARTER_ONE:
                startMonth = "JANUARY";
                endMonth = "MARCH";
                break;
            case Constants.QUARTER_TWO:
                startMonth = "APRIL";
                endMonth = "JUNE";
                break;
            case Constants.QUARTER_THREE:
                startMonth = "JULY";
                endMonth = "SEPTEMBER";
                break;
            case Constants.QUARTER_FOUR:
                startMonth = "OCTOBER";
                endMonth = "DECEMBER";
                break;
            case Constants.IN_YEAR:
                startMonth = "JANUARY";
                endMonth = "DECEMBER";
                break;
            default:
                break;
        }
        quarterRanges.setQuarter(quarter);
        quarterRanges.setStartMonth(startMonth);
        quarterRanges.setEndMonth(endMonth);

        return quarterRanges;
    }

}

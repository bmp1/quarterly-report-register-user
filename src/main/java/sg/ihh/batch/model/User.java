package sg.ihh.batch.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    @JsonProperty("mail")
    private String mail;

    @JsonProperty("pplID")
    private String pplID;

    @JsonProperty("createTimestamp")
    private String createTimestamp;

    public User() {
        // Empty Constructor
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPplID() {
        return pplID;
    }

    public void setPplID(String pplID) {
        this.pplID = pplID;
    }

    public String getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(String createTimestamp) {
        this.createTimestamp = createTimestamp;
    }
}


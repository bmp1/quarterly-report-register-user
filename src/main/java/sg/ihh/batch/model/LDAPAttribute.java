package sg.ihh.batch.model;

public class LDAPAttribute {

    private LDAPAttribute() {}

    public static final String CN     = "cn";
    public static final String SN     = "sn";
    public static final String MOBILE = "mobile";
    public static final String MAIL   = "mail";
    public static final String UID = "uid";

    public static final String CREATE_TIME_STAMP = "createtimestamp";
    public static final String LAST_LOGIN_TIME   = "lastlogintime";
    public static final String PWD_LAST_CHANGED  = "pwdChangedTime";

    public static final String NRIC              = "pplID";
    public static final String EMPLOYEE_TYPE     = "employeeType";
    public static final String COUNTRY_CODE      = "pplCountryCode";
    public static final String DOB               = "pplDOB";
    public static final String GENDER            = "pplGender";

    public static final String COUNTRY           = "pplcountry";
    public static final String NATIONALITY       = "pplnationality";

    public static final String USER_PASSWORD     = "userPassword";
}

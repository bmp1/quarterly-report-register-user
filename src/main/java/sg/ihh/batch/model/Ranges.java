package sg.ihh.batch.model;

public class Ranges {

    private String quarter;
    private String createDt;
    private String startMonth;
    private String endMonth;
    private String year;
    private String fileOutputName;
    private String fileOutputLocation;
    private String remarks;

    public Ranges() {
        // Empty constructor
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(String startMonth) {
        this.startMonth = startMonth;
    }

    public String getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(String endMonth) {
        this.endMonth = endMonth;
    }

    public String getFileOutputName() {
        return fileOutputName;
    }

    public void setFileOutputName(String fileOutputName) {
        this.fileOutputName = fileOutputName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getFileOutputLocation() {
        return fileOutputLocation;
    }

    public void setFileOutputLocation(String fileOutputLocation) {
        this.fileOutputLocation = fileOutputLocation;
    }
}

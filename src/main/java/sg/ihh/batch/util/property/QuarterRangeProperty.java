package sg.ihh.batch.util.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix="range")
@Validated
public class QuarterRangeProperty extends BaseProperty {

    private boolean quarterCustom;
    private String quarterCustomValue;
    private String quarterCustomYear;

    public QuarterRangeProperty() {
        log = getLogger(QuarterRangeProperty.class);
        log.info("QuarterRangeProperty Initiated");
    }

    public boolean isQuarterCustom() {
        return quarterCustom;
    }

    public void setQuarterCustom(boolean quarterCustom) {
        this.quarterCustom = quarterCustom;
    }

    public String getQuarterCustomValue() {
        return quarterCustomValue;
    }

    public void setQuarterCustomValue(String quarterCustomValue) {
        this.quarterCustomValue = quarterCustomValue;
    }

    public String getQuarterCustomYear() {
        return quarterCustomYear;
    }

    public void setQuarterCustomYear(String quarterCustomYear) {
        this.quarterCustomYear = quarterCustomYear;
    }
}

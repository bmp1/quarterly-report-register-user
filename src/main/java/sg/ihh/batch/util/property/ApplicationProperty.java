package sg.ihh.batch.util.property;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix="app")
@Validated
public class ApplicationProperty extends BaseProperty {

    @JsonProperty("outputLocation")
    private String outputLocation;
    @JsonProperty("outputFilename")
    private String outputFilename;

    public ApplicationProperty() {
        log = getLogger(ApplicationProperty.class);
        log.info("ApplicationProperty Initiated");
    }

    public String getOutputLocation() {
        return outputLocation;
    }

    public void setOutputLocation(String outputLocation) {
        this.outputLocation = outputLocation;
    }

    public String getOutputFilename() {
        return outputFilename;
    }

    public void setOutputFilename(String outputFilename) {
        this.outputFilename = outputFilename;
    }
}

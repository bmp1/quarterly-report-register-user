package sg.ihh.batch.util.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix="mail")
@Validated
public class MailProperty extends BaseProperty {

    private String userSource;
    private boolean doLog;
    private boolean processCountry;
    private boolean processNationality;


    public MailProperty() {
        log = getLogger(MailProperty.class);
        log.info("ApplicationProperty Initiated");
    }

    public String getUserSource() {
        return userSource;
    }

    public void setUserSource(String userSource) {
        this.userSource = userSource;
    }

    public boolean isDoLog() {
        return doLog;
    }

    public void setDoLog(boolean doLog) {
        this.doLog = doLog;
    }

    public boolean isProcessCountry() {
        return processCountry;
    }

    public void setProcessCountry(boolean processCountry) {
        this.processCountry = processCountry;
    }

    public boolean isProcessNationality() {
        return processNationality;
    }

    public void setProcessNationality(boolean processNationality) {
        this.processNationality = processNationality;
    }
}

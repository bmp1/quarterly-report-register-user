package sg.ihh.batch.util.property;

public class Constant {

    private Constant() {}

    public static final String COMMON_PROPERTY_FILENAME         = "application.common.properties";

    public static final String PROPERTY_FILENAME                = "application.properties";

    // LDAP Default
    public static final String LDAP_SEARCH_REGISTER_FILTER            = "createTimestamp";


}

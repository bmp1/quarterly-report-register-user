package sg.ihh.batch.util.ldap;

import com.unboundid.ldap.sdk.*;
import com.unboundid.ldap.sdk.controls.SimplePagedResultsControl;
import org.apache.directory.api.ldap.model.password.PasswordUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LDAPHelper {

    private LDAPHelper() {
    }

    public static Optional<SearchResultEntry> performSearch(LDAPConnection conn, Filter filter, String baseDn,
                                                            String attributes) throws LDAPSearchException {

        Optional<SearchResultEntry> entry = Optional.empty();

        SearchRequest searchRequest = new SearchRequest(baseDn, SearchScope.SUB, filter, attributes, SearchRequest.ALL_OPERATIONAL_ATTRIBUTES);

        SearchResult result = conn.search(searchRequest);

        if (result.getEntryCount() > 0) {
            entry = Optional.of(result.getSearchEntries().get(0));
        }
        return entry;
    }

    public static List<SearchResultEntry> paginationSearch(LDAPConnection conn, Filter filter, String baseDn,
                                                           String attributes) throws LDAPSearchException {

        List<SearchResultEntry> entry = new ArrayList<>();

        SearchRequest searchRequest = new SearchRequest(baseDn, SearchScope.SUB, filter, attributes, SearchRequest.ALL_OPERATIONAL_ATTRIBUTES);
        //searchRequest.setControls(new SimplePagedResultsControl(10000, null));
        //searchRequest.setSizeLimit(0);

        SearchResult result = conn.search(searchRequest);
        if (result.getEntryCount() > 0) {
            entry = result.getSearchEntries();
        }
        return entry;
    }

    public static boolean checkBind(LDAPConnection conn, SearchResultEntry entry, String password)
            throws LDAPException {
        BindResult bindResult = conn.bind(entry.getDN(), password);
        return bindResult.getResultCode() == ResultCode.SUCCESS;
    }

    public static boolean compareCredentials(String plainCredentials, String hashedCredentials) {

        // hashedCredentials is hashed password stored in LDAP
        return PasswordUtil.compareCredentials(plainCredentials.getBytes(), hashedCredentials.getBytes());
    }

}

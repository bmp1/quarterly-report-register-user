package sg.ihh.batch.util.helper;

import sg.ihh.batch.util.log.AppLogger;

public class BaseHelper {

    protected static AppLogger log;
    public BaseHelper() {
        // Empty Constructor
    }
    protected static AppLogger getLogger(Class<?> clazz) {
        return new AppLogger(clazz);
    }

    protected void start(String methodName) {
        log.debug(methodName, "Start");
    }

    protected void completed(String methodName) {
        log.debug(methodName, "Completed");
    }

}

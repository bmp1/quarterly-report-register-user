package sg.ihh.batch.util.helper;

import org.apache.commons.io.FileUtils;
import sg.ihh.batch.util.log.AppLogger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileHelper {

    private static AppLogger log = new AppLogger(FileHelper.class);
    private static final String PATH_DELIMETER = "/";

    private FileHelper() {
        //Empty constructor
    }

    public static boolean createOutputFile(String destinationPath, String outputFileName) {
        String methodName = "createOutputFile";

        boolean create = false;
        String rootPath = createDirectory(destinationPath);
        log.debug(methodName, rootPath);
        if (rootPath != null) {
            File dest = createFile(rootPath, outputFileName);
            try {
                if (dest.exists()) {
                    Files.deleteIfExists(dest.toPath());
                }
                Files.createFile(dest.toPath());
                create = true;
            } catch (IOException e) {
                log.error(methodName, e);
                create = false;
            }
            log.debug(methodName, "Create file: " + dest.getAbsolutePath());
        }
        return create;
    }

    private static File createFile(String rootPath, String outputFileName) {
        String destination = rootPath + PATH_DELIMETER + outputFileName;
        return new File(PATH_DELIMETER + destination.replace("\\", "/"));
    }

    public static String createDirectory(String destinationPath) {
        String methodName = "createDirectory";
        boolean isCreated = false;

        String rootPath = new File("").getAbsolutePath().concat(PATH_DELIMETER + destinationPath);
        File destinationFile = new File(PATH_DELIMETER + rootPath);

        if (!destinationFile.exists()) {
            if (destinationFile.mkdir()) {
                log.debug(methodName, "Folder is created");
                isCreated = true;
            } else {
                log.error(methodName, "Failed to create folder");
            }
        } else {
            isCreated = true;
        }
        if (!isCreated) {
            return null;
        }
        return rootPath.replace("\\", "/");
    }

    public static void writeToFile(String content, String filePath) {
        final String methodName = "writeToFile";
        try {
            File file = new File(filePath);
            java.io.FileWriter fw = new java.io.FileWriter(file.getAbsoluteFile());
            try (BufferedWriter bw = new BufferedWriter(fw)) {
                // Write in file
                bw.write(content);
                // Close connection
            }
        } catch (IOException e) {
            log.error(methodName, e.getMessage());
        }
    }

    public static void copyTo(String source, String destination, String outputFileName) {
        final String methodName = "copyTo";

        String rootPath = createDirectory(destination);
        File sourceFiles = new File(source);
        File outputFiles = createFile(rootPath, outputFileName);
        try {
            if (outputFiles.exists()) {
                Files.deleteIfExists(outputFiles.toPath());
            }
            FileUtils.copyFile(sourceFiles, outputFiles);
        } catch (IOException e) {
            log.error(methodName, e.getMessage());
        }
    }
}

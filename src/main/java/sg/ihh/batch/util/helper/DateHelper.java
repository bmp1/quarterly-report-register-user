package sg.ihh.batch.util.helper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateHelper {

    public static final DateTimeFormatter DT_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter D_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter EXCEL_FORMATTER = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
    public static final DateTimeFormatter TIMESTAMP_FORMATTER = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss");
    public static final DateTimeFormatter DATESTAMP_FORMATTER = DateTimeFormatter.ofPattern("ddMMyyyy");
    public static final DateTimeFormatter LDAP_DT_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss'Z'");

    private DateHelper() {
        // Empty Constructor
    }

    public static String formatDateTime(LocalDateTime dt) {
        if (dt != null) {
            return dt.format(DT_FORMATTER);
        }
        return null;
    }

    public static String formatDate(LocalDateTime dt) {
        if (dt != null) {
            return dt.format(D_FORMATTER);
        }
        return null;
    }

    public static String formatDate(LocalDate dt) {
        if (dt != null) {
            return dt.format(D_FORMATTER);
        }
        return null;
    }

    public static String formatTimestamp(LocalDateTime dt) {
        if (dt != null) {
            return dt.format(TIMESTAMP_FORMATTER);
        }
        return null;
    }

    public static String formatDatestamp(LocalDateTime dt) {
        if (dt != null) {
            return dt.format(DATESTAMP_FORMATTER);
        }
        return null;
    }

    public static String formatExcelDatestamp(LocalDateTime dt) {
        if (dt != null) {
            return dt.format(EXCEL_FORMATTER);
        }
        return null;
    }

    public static String formatLDAPFormatter(LocalDateTime dt) {
        if (dt != null) {
            return dt.format(LDAP_DT_FORMATTER);
        }
        return null;
    }

    public static LocalDateTime parseLdapFormat(String str) {
         return LocalDateTime.parse(str, LDAP_DT_FORMATTER);
    }

    public static LocalDateTime parseDateTime(String str) {
        return LocalDateTime.parse(str, DT_FORMATTER);
    }

    public static LocalDate parseDate(String str) {
        return LocalDate.parse(str, D_FORMATTER);
    }
}

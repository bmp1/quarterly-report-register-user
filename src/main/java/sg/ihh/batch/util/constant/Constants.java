package sg.ihh.batch.util.constant;

public class Constants {

    private Constants() {
        // Empty Constructor
    }

    // Status
    public static final String START = "Start";
    public static final String END = "Completed";

    public static final String PARAM_JOB_ID = "JobID";

    public static final String QUARTER_ONE = "Q1";
    public static final String QUARTER_TWO = "Q2";
    public static final String QUARTER_THREE = "Q3";
    public static final String QUARTER_FOUR = "Q4";
    public static final String IN_YEAR = "ALL";


}

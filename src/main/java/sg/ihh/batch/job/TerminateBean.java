package sg.ihh.batch.job;

import sg.ihh.batch.util.log.AppLogger;

import javax.annotation.PreDestroy;

public class TerminateBean {

    AppLogger log = new AppLogger(this.getClass());
    @PreDestroy
    public void onDestroy()  {
        log.debug("Spring Container is destroyed!");
    }
}
package sg.ihh.batch.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sg.ihh.batch.manager.LDAPConnectionManager;
import sg.ihh.batch.model.Ranges;
import sg.ihh.batch.model.User;
import sg.ihh.batch.tasklet.*;
import sg.ihh.batch.util.property.*;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class QuarterlyReportJob {

    @Autowired
    public StepBuilderFactory stepBuilder;

    private Ranges quarterRanges = new Ranges();

    @Autowired
    private ApplicationProperty applicationProperty;

    @Autowired
    private QuarterRangeProperty quarterRangeProperty;

    @Autowired
    protected LdapProperty ldapProperty;

    @Autowired
    private LDAPConnectionManager ldapConnectionManager;

    private List<User> userList = new ArrayList<>();

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    public QuarterlyReportJob(JobBuilderFactory jobBuilder, StepBuilderFactory stepBuilder) {
        this.stepBuilder = stepBuilder;
    }


    @Bean
    Job job() {
        return this.jobBuilderFactory.get("quarterlyReportJob")
                .start(quarterRangeConfigureStep())
                .next(downloadUserFromLdapStep())
                .next(generateReportStep())
                .build();
    }

    @Bean
    @Qualifier("quarterRangeConfigure")
    public Step quarterRangeConfigureStep() {
        return this.stepBuilder.get("quarterRangeConfigureStep")
                .tasklet(new QuarterRangeConfigureTasklet(applicationProperty, quarterRangeProperty, quarterRanges,userList))
                .build();
    }

    @Bean
    @Qualifier("downloadUserFromLdap")
    public Step downloadUserFromLdapStep() {
        return this.stepBuilder.get("downloadUserFromLdapStep")
                .tasklet(new DownloadUserFromLdapTasklet(quarterRanges, ldapProperty,ldapConnectionManager, userList))
                .build();
    }

    @Bean
    @Qualifier("generateReport")
    public Step generateReportStep() {
        return this.stepBuilder.get("generateReportStep")
                .tasklet(new GenerateReportTasklet(quarterRanges, userList))
                .build();
    }


}